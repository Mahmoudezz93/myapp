import { createStackNavigator } from 'react-navigation-stack';
import welcomePage from "../screens/welcomePage";
import secondPage from "../screens/secondPage"

export default createStackNavigator({
  Welcome: {screen : welcomePage},
  Second : {screen: secondPage },
 }, {
  initialRouteName: 'Welcome',
});