import React, { Component } from 'react';
import { Button, View ,Text,Container,Content,Header,StyleProvider} from 'native-base';
import { AppLoading } from 'expo';
import * as Font  from 'expo-font';
import { Ionicons,MaterialCommunityIcons,MaterialIcons,Entypo,FontAwesome } from '@expo/vector-icons';
 import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/material';
import { createAppContainer } from 'react-navigation';
import AppNavigator from"./navigation/AppContainer.js";
const AppContainer = createAppContainer(AppNavigator);

import {
  StatusBar
} from"react-native";
import { Platform ,Dimensions} from 'react-native';
const platform = Platform.OS;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;


export default class App extends  Component {

constructor(props) {
  super(props);
  this.state = {
    isReady: false,
  };
}
async componentDidMount() {
  await Font.loadAsync({
    Roboto: require('native-base/Fonts/Roboto.ttf'),
    Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
    ...Ionicons.font,
    ...MaterialCommunityIcons.font,
    ...MaterialIcons.font,
    ...Entypo.font,
    ...FontAwesome.font
  });
  this.setState({ isReady: true });
}
render() {
  if (!this.state.isReady) {
    return <AppLoading />;
  }
  return (
    <StyleProvider style={getTheme(material)}>
    <Container style={{ backgroundColor:"#000" } }> 
    <StatusBar barStyle="light-content" style={{height:0 ,padding:0}}/>
    <AppContainer/>
    </Container>
    </StyleProvider>
  );
}
}